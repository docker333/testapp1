#!/usr/bin/perl

{
package CookieServ;
 
use HTTP::Server::Simple::CGI;
use base qw(HTTP::Server::Simple::CGI);
use JSON;
 
sub findcontainer {
  $myuser = shift;
  if ($myuser =~ m/\d\d\d\d\d\d/) {
    my $dbfile = '/opt/docker/db/database.txt';
    my $isuserexist = false;

    # read database (simple text file)
    if (-f $dbfile) {
      open (FHREAD, $dbfile)
        or die "Could not open file '$dbfile' $!";
      # file exist and opened, reading entries match  userid:docker_cont_id
      while(<FHREAD>) {
        chomp;              # remove trailing newline characters
        push @rawdata, $_;  # push the data line onto the array
      }
      close(FHREAD);

      for ($i=0; $i<scalar(@rawdata); $i++) {
        if(@rawdata[$i] =~ m/(\w+):(\w+)/) {
          $db_userid = $1;
          $db_contid = $2;

          if (($db_userid eq $myuser) and ($isuserexist eq false)) {
            $isuserexist = true;
            print "Cookie found. NGINX redirected control to HOST application.<br>";
            print "This is EXISTING user (db record found), container already exist, requesting current status...<br><hr><br>";
            print "Docker container found, checking status...<br><br>";

            $docker_inspect = `/usr/bin/docker inspect $db_contid | awk \'NR>2 {print l} {l=\$0}\'`;
            my $content = decode_json($docker_inspect);

            print '<table table border="1" style="border-collapse:collapse">';
            print '<tr><td>Name:</td><td>'.$content->{'Name'}.'</td></tr>';
            print '<tr><td>Id:</td><td>'.$content->{'Id'}.'</td></tr>';
            print '<tr><td>Created:</td><td>'.$content->{'Created'}.'</td></tr>';
            print '<tr><td>Image:</td><td>'.$content->{'Image'}.'</td></tr>';
            print '<tr><td>State -> Status:</td><td>'.$content->{'State'}{'Status'}.'</td></tr>';
            print '<tr><td>State -> StartedAt:</td><td>'.$content->{'State'}{'StartedAt'}.'</td></tr>';
            print '</table>';
            print '<br>Request completed for user '.$myuser."\n\n";
          }
        }
      }
    }

    # check if user found in db after read, if not - create run new docker container and add to DB file
    if ($isuserexist eq false) { 
      open(my $FHWRITE, '>>', $dbfile)
        or die "Could not open file '$dbfile' for writing $!";

      $runtmp2 = `/usr/bin/docker run -i -t --name user$myuser -d userspace /bin/bash`;
    
      print "Cookie found. NGINX redirected control to HOST application.<br>";
      print "This is NEW user, starting NEW container and keep it running.<br><hr><br>";
      print "Container <b>$runtmp2</b> created, up and running for user <b>$myuser</b>, entry saved to database.<br>";
      print "Please refresh page for running status.<br>";
      print $FHWRITE "$myuser:$runtmp2";
      close($FHWRITE);
    }
  }
}
  
sub handle_request {
  my $self = shift;
  my $cgi  = shift;
   
  my $userid = $cgi->path_info();
  $userid =~ s/\///g;
 
  print "HTTP/1.0 200 OK\r\n";
  print $cgi->header;
  print $cgi->escapeHTML(findcontainer($userid));
}


} 
my $pid = CookieServ->new(8080)->background();
