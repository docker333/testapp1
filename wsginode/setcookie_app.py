from cgi import parse_qs
from wsgiref.simple_server import make_server
from random import randint

def cookie_app(environ, start_response):
        response_cookie = 'userid=' + str(randint(100000,999999))

        status = '200 OK'
	response_body = 'Cookie NOT found. NGINX redirected control to WSGI app (running in container).<br><hr><br>'
        response_body = response_body + 'cookie set OK ('+ response_cookie + ')'
        response_body = response_body + '<br><br>Please refresh page or visit this page later.'

        response_headers = [('Set-Cookie', response_cookie),
                            ('Content-Type', 'text/html'),
                            ('Content-Length', str(len(response_body)))]
        start_response(status, response_headers)

        return [response_body]

httpd = make_server('', 1888, cookie_app)
print "running port 1888"
httpd.serve_forever()

